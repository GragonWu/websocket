package com.dragonwu.im.constant;

/**
 * @author Dragon Wu
 * @since 2023/1/23 17:02
 * IM服务的常量
 */
public interface IMConstants {
    //websocket的访问路径
    String WEBSOCKET_PATH = "/websocket";
    //最大传输大小
    int MAX_CONTENT_LENGTH = 65536;
    //websocket的host地址
    String HOST = "ws://localhost:";
}
