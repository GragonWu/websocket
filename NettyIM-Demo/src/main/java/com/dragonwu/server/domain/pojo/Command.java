package com.dragonwu.server.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author DragonWu
 * @since 2023-01-05 14:42
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Command {

    //连接信息编码
    private Integer code;

    //用户昵称
    private String nickname;
}
