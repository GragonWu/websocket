package com.dragonwu.server.handler;

import com.dragonwu.server.IMServer;
import com.dragonwu.server.domain.Result;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author DragonWu
 * @since 2023-01-06 13:24
 **/
public class JoinGroupHandler {
    public static void execute(ChannelHandlerContext channelHandlerContext){
        //将Channel添加到ChannelGroup
        IMServer.GROUP.add(channelHandlerContext.channel());
        channelHandlerContext.channel().writeAndFlush(Result.success("加入系统默认群聊成功~"));
    }
}
