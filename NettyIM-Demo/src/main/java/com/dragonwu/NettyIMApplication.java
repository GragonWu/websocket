package com.dragonwu;

import com.dragonwu.server.IMServer;

/**
 * @author DragonWu
 * @since 2023-01-05 14:01
 **/
public class NettyIMApplication {
    public static void main(String[] args) {
        IMServer.start();
    }
}
