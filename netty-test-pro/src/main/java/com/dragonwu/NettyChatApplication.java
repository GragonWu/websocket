package com.dragonwu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Dragon Wu
 * @since 2022-10-17 13:06
 **/
@SpringBootApplication
public class NettyChatApplication {
    public static void main(String[] args) throws Exception {
//        SpringApplication.run(NettyChatApplication.class, args);
        ConfigurableApplicationContext context =
                SpringApplication.run(NettyChatApplication.class, args);

        ChatServerStarter chatServerStarter = context.getBean(ChatServerStarter.class);
        chatServerStarter.start();
    }
}
